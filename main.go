package main

import (
	"fmt"
	"os"

	"example.com/order-service/db"
	"example.com/order-service/routes"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	amqp "github.com/rabbitmq/amqp091-go"
)

func main() {
	// load .env
	godotenv.Load(".env")

	// set GIN_MODE
	gin.SetMode(os.Getenv("GIN_MODE"))

	// connect to postgres DB
	db.ConnectDB()

	// connect to rabbitmq
	connectRabbitMQ()

	r := gin.Default()

	//allow CORS
	r.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"*"},
		AllowHeaders:     []string{"*"},
		ExposeHeaders:    []string{"*"},
		AllowCredentials: true,
	}))

	//localhost:6000/api/v1
	apiV1Route := r.Group("/api/v1")

	//Home Route
	routes.InitHomeRoute(apiV1Route)

	//Order Route
	routes.InitOrderRoute(apiV1Route)

	r.Run(":" + os.Getenv("PORT")) // localhost:6000/api/v1
}

func failOnError(err error, msg string) {
	if err != nil {
		fmt.Printf("%s: %s", msg, err)
	}
}

// connect rabbitmq
func connectRabbitMQ() {
	conn, err := amqp.Dial(os.Getenv("RABBITMQ_URL"))
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	msgs, err := ch.Consume("q.akenarin.order.service", "", true, false, false, false, nil)
	failOnError(err, "Failed to declare a queue")

	var forever chan struct{}

	go func() {
		for d := range msgs {
			fmt.Printf("Received a message: %s", d.Body)

			// fmt.Printf("Done")
			// d.Ack(true)
		}
	}()

	fmt.Println(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}
